import java.util.Scanner;

public class TimeStore {

    private int hour;
    private int minute;
    private int second;
    private String h, m, s;
    private String tripID;
    private String departureTime;
    private String stopId;
    private String stopSequence;
    private String stopHeadSign;
    private String pickupType;
    private String dropOffType;
    private String shapeDistTraveled;
    private String arrivalTime;


    TimeStore(String tripID, String arrivalTime, String departureTime, String stopID, String stopSequence, String stopHeadSign, String pickupType, String
              dropOffType, String shapeDistTraveled){
        this.tripID = tripID;
        this.arrivalTime = arrivalTime.trim();
        this.departureTime = departureTime;
        this. stopId = stopID;
        this.stopSequence = stopSequence;
        this.stopHeadSign = stopHeadSign;
        this.pickupType = pickupType;
        this.dropOffType = dropOffType;
        this. shapeDistTraveled = shapeDistTraveled;

        Scanner separate = new Scanner(arrivalTime);
        separate.useDelimiter(":");
        h = separate.next();
        hour = Integer.parseInt(h.trim());
        m = separate.next();
        minute = Integer.parseInt(m.trim());
        s = separate.next();
        second = Integer.parseInt(s.trim());
    }

    String getArrivalTime(){return arrivalTime;}

    int getTripId(){
        return Integer.parseInt(tripID);
    }

    boolean isValid(){
        return hour < 24 && minute < 60 && second < 60;
    }

    String output(){
        return tripID + ", " + arrivalTime  + ", " + departureTime + ", " + stopId + ", " + stopHeadSign + ", " +
                pickupType + ", " + dropOffType + ", " + shapeDistTraveled;
    }

    public String getTripID() {
        return tripID;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public String getStopId() {
        return stopId;
    }

    public String getStopHeadSign() {
        return stopHeadSign;
    }

    public String getPickupType() {
        return pickupType;
    }

    public String getDropOffType() {
        return dropOffType;
    }

    public String getShapeDistTraveled() {
        return shapeDistTraveled;
    }

    public String getStopSequence() {
        return stopSequence;
    }
}
