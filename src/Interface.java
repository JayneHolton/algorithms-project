import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Interface {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        int input = 0;
        boolean exit = true;
        while (exit) {

            System.out.print("\n\n");
            System.out.println("This program searches through the Vancouver bus network data.\n" +
                    "Please select an option from the list below by typing the corresponding number into the command line.\n" +
                    "Or type 'exit' to quit the program.");
            System.out.println("1. Search for all trips with a given arrival time.");
            System.out.println("2. Search for the optimal route between two stops.");
            System.out.println("3. Search for stop information by name of stop.");
            String isExit = "";
            boolean wait = true;
            while (wait) {
                if (scanner.hasNextInt()) {
                    input = scanner.nextInt();
                    if (input < 1 || input > 3) {
                        System.out.println("Please enter an input in the specified format.");
                    } else wait = false;
                } else if (scanner.hasNext()) {
                    isExit = scanner.next();
                    if(isExit.equals("exit")){
                        System.out.println("Goodbye.");
                        break;
                    }
                    System.out.println("Please enter an input in the specified format.");
                }
            }
            if(isExit.equals("exit")){
                break;
            }

            wait = true;

            if (input == 1) {
                String arrivalTime = "";
                Pattern timeFormat1 = Pattern.compile("\\d{2}:\\d{2}:\\d{2}");
                Pattern timeFormat2 = Pattern.compile("\\d{1}:\\d{2}:\\d{2}");

                while (wait) {
                    System.out.print("Please enter the arrival time of the trips you wish to find in the format hh:mm:ss:");
                    if (scanner.hasNext()) {
                        arrivalTime = scanner.next();
                        if (timeFormat1.matcher(arrivalTime).matches()) {
                            TripSearch tripSearch = new TripSearch("stop_times.txt", arrivalTime);
                            tripSearch.printFoundTimes();
                            System.out.println("\n\n\n");
                            wait = false;
                        }
                        else if (timeFormat2.matcher(arrivalTime).matches()) {
                            TripSearch tripSearch = new TripSearch("stop_times.txt", arrivalTime);
                            tripSearch.printFoundTimes();
                            System.out.println("\n\n\n");
                            wait = false;
                        } else{
                            System.out.println("Please enter an input in the given format");
                        }
                    }
                }
            } else if(input == 2){
                Scanner scanner2 = new Scanner(System.in);
                int busStop1 = 0;
                int busStop2 = 0;

                System.out.println("Please enter the two bus stop numbers you wish to find a route between.");
                System.out.print("1:");
                while (wait) {

                    if (scanner.hasNextInt()) {
                        busStop1 = scanner.nextInt();
                        wait = false;
                    } else if (scanner.hasNext()) {
                        System.out.println("Please enter an input in the given format");
                        System.out.print("1:");
                    }
                }
                wait = true;
                System.out.print("2:");
                while (wait) {

                    if (scanner.hasNextInt()) {
                        busStop2 = scanner.nextInt();
                        wait = false;
                    } else if (scanner.hasNext()) {
                        System.out.println("Please enter an input in the given format");
                        System.out.print("2:");
                    }
                }
                ShortestRoute.shortestPath(busStop1, busStop2);
            } else if(input == 3){
                wait = true;
                while(wait) {
                    StopSearch shortestPath = new StopSearch();
                    shortestPath.busStopSearch();
                    wait = false;
                }
            }

        }
    }
}
