import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Time;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TripSearch {

    private String filename;
    private TimeStore[] arrivalTimes;
    private ArrayList<TimeStore> matches = new ArrayList<TimeStore>();
    private TimeStore[] arrivalTimesMatched;
    private static TimeStore[] stopTimes;
    private static int length;
    private List<List<String>> table;


    TripSearch(String filename, String requestedTime) throws FileNotFoundException {
        File file = new File(filename);
        Scanner scanner = new Scanner(file);
        int i = 0;
        scanner.useDelimiter(",");
        TimeStore temp;
        String requestedTime2 = formatTime(requestedTime);
        ArrayList<TimeStore> arrivalTimesTemp = new ArrayList<TimeStore>();


        while (scanner.hasNextLine()) {
            String[] line = scanner.nextLine().trim().split(",");
            if (line.length < 9 && i > 0) {
                temp = new TimeStore(line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7], "");
                if (temp.isValid()) {
                    arrivalTimesTemp.add(temp);
                }
            } else if (i > 0) {
                temp = new TimeStore(line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7], line[8]);
                if (temp.isValid()) {
                    arrivalTimesTemp.add(temp);
                }
            }
            i++;
        }
        int size = arrivalTimesTemp.size();

        arrivalTimes = arrivalTimesTemp.toArray(new TimeStore[0]);
        for(int r = 0; r < size; r++) {
            if (requestedTime2.equals(arrivalTimes[r].getArrivalTime())) {
                matches.add(arrivalTimes[r]);
            }
        }

        length = matches.size();
        arrivalTimesMatched = matches.toArray(new TimeStore[0]);
        IdSort sort = new IdSort(arrivalTimesMatched, length);
        stopTimes = sort.getSortedArray();



    }

    void printFoundTimes() {
        if (length > 0) {
            ArrayList<String> tripID = new ArrayList<String>();
            tripID.add("trip_id");
            ArrayList<String> arrivalTime = new ArrayList<String>();
            arrivalTime.add("arrival_time");
            ArrayList<String> departureTime = new ArrayList<String>();
            departureTime.add("departure_time");
            ArrayList<String> stopId = new ArrayList<String>();
            stopId.add("stop_id");
            ArrayList<String> stopSequence = new ArrayList<String>();
            stopSequence.add("stop_sequence");
            ArrayList<String> stopHeadsign = new ArrayList<String>();
            stopHeadsign.add("stop_headsign");
            ArrayList<String> pickupTime = new ArrayList<String>();
            pickupTime.add("pickup_time");
            ArrayList<String> dropOffTime = new ArrayList<String>();
            dropOffTime.add("drop_off_time");
            ArrayList<String> shapeDistTraveled = new ArrayList<String>();
            shapeDistTraveled.add("shape_dist_traveled");

            for (int i = 0; i < length; i++) {
                tripID.add(stopTimes[i].getTripID());
                arrivalTime.add(stopTimes[i].getArrivalTime());
                departureTime.add(stopTimes[i].getDepartureTime());
                stopId.add(stopTimes[i].getStopId());
                stopSequence.add(stopTimes[i].getStopSequence());
                stopHeadsign.add(stopTimes[i].getStopHeadSign());
                pickupTime.add(stopTimes[i].getPickupType());
                dropOffTime.add(stopTimes[i].getDropOffType());
                shapeDistTraveled.add(stopTimes[i].getShapeDistTraveled());
            }
            table = Arrays.asList(tripID, arrivalTime, departureTime, stopId,
                    stopSequence, stopHeadsign, pickupTime, dropOffTime, shapeDistTraveled);

            PrintAsTable(table);
        } else System.out.println("There were no entries matching the given arrival time.");
    }

    void PrintAsTable(List<List<String>> table) {
        List<Integer> maxLengths = findMaxLengths();

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < table.get(0).size(); i++) {
            for (int j = 0; j < table.size(); j++) {
                String currentValue = table.get(j).get(i);
                sb.append(currentValue);
                for (int k = 0; k < (maxLengths.get(j) - currentValue.length() + 3); k++) {
                    sb.append(' ');
                }
            }
            sb.append('\n');
        }

        System.out.println(sb);
    }

    private List<Integer> findMaxLengths() {
        List<Integer> maxLengths = new ArrayList<>();
        for (List<String> row : table) {
            int maxLength = 0;
            for (String value : row) {
                if (value.length() > maxLength) {
                    maxLength = value.length();
                }
            }
            maxLengths.add(maxLength);
        }
        return maxLengths;
    }

    private String formatTime(String requestedTime){

        Scanner separate = new Scanner(requestedTime);
        separate.useDelimiter(":");
        String h = separate.next();
        int hour = Integer.parseInt(h.trim());
        String m = separate.next();
        int minute = Integer.parseInt(m.trim());
        String s = separate.next();
        int second = Integer.parseInt(s.trim());

        String newRequestedTime = "" + hour + ":" + m + ":" + s;
        return newRequestedTime;

    }

}
