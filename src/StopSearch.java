import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class StopSearch {
    private tst ternarySearchTree = new tst();
    private ArrayList<BusStopData> busStops = new ArrayList<BusStopData>();

    public  void busStopSearch() throws FileNotFoundException {
        objectInitiation();
        System.out.println("Please enter the name(or part of the name) of the stop you are searching for.");
        Scanner input = new Scanner(System.in);
        String userInput = input.nextLine().toUpperCase();
        if(userInput.equals("") || userInput.equals(" "))
        {
            System.out.println("Invalid input.");
            busStopSearch();
        } else if(ternarySearchTree.toString().contains(userInput))
        {
            System.out.println("The following stops met your search criteria: \nstop_id,stop_code,stop_name,stop_desc,stop_lat,stop_lon,zone_id,stop_url,location_type,parent_station");
            for(int count=0;count<busStops.size();count++)
            {
                if(busStops.get(count).toString().contains(userInput))
                {
                    System.out.println(busStops.get(count).toString());
                }
            }
        }
        else
            System.out.println("There are no stops that meet your search criteria.");
    }


    public  void objectInitiation() throws FileNotFoundException {
        File f = new File("stops.txt");
        String currentLine = "";
        Scanner x = new Scanner(f);
        int i = 0;
        String rearrangedStopName = "";
        while (x.hasNextLine()) {
            currentLine = x.nextLine();
            if(i > 0) {
                String[] data = currentLine.split("[,]",0);
                rearrangedStopName = rearrangeStopName(data[2]);
                ternarySearchTree.insert(rearrangedStopName);
                busStops.add(new BusStopData(data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7],data[8],"",rearrangedStopName));
            }
            i++;
        }
        x.close();
    }

    String rearrangeStopName(String stopName) {
        String rearrangedStopName = "";
        String tempString1 = "";
        String tempString2 = "";
        String[] subStrings = stopName.split("[ ]", 0);
        tempString1 = subStrings[0];
        for (int count = 1; count < subStrings.length; count++)
        {
            tempString2 = tempString2 + subStrings[count] + " ";
        }
        rearrangedStopName = tempString2 + tempString1;
        return rearrangedStopName;
    }
}
