import java.util.Hashtable;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Graph {
	Hashtable<Integer, Hashtable<Integer,Double>> hashTable;
	int V;
	int E;
	
	Graph(String stopsFile, String stopTimesFile, String transfersFile) throws FileNotFoundException
	{
		Hashtable<Integer, Hashtable<Integer, Double>> ht = new Hashtable<>();
		this.hashTable = ht;
		
		
		//Add stops from stops file
		
		File stops = new File(stopsFile);
		Scanner scan = new Scanner(stops);
		scan.useDelimiter(",");
		if(scan.hasNextLine())
		{
			scan.nextLine();
		}
		
		while(scan.hasNextLine())
		{
			int stopNumber = scan.nextInt();
			Hashtable<Integer, Double> stopTable = new Hashtable<>();
			this.hashTable.put(stopNumber, stopTable);
			scan.nextLine();
			this.V++;
		}
		scan.close();
		
		//Add edges from stopTimes file
		
		File stopTimes = new File(stopTimesFile);
		scan = new Scanner(stopTimes);
		scan.useDelimiter(",");
		if(scan.hasNextLine())
		{
			scan.nextLine();
		}
		
		
		int currentTripID = 0;
		int lastTripID = 0;
		int currentStop = 0;
		int lastStop = 0;
		while(scan.hasNextLine())
		{
			currentTripID = scan.nextInt();
			scan.next();
			scan.next();
			currentStop = scan.nextInt();
			scan.nextLine();
			if (currentTripID == lastTripID)
			{
				Hashtable<Integer, Double> stopTable = new Hashtable<>();
				stopTable = this.hashTable.get(lastStop);
				stopTable.put(currentStop, 1.0);
				this.hashTable.put(lastStop, stopTable);
				this.E++;
			}
			lastTripID = currentTripID;
			lastStop = currentStop;
		}
		scan.close();
		
		
		//Add edges from transfers file
		
		File transfers = new File(transfersFile);
		scan = new Scanner(transfers);
		scan.useDelimiter(",|\\n");	
		
		int source = 0;
		int destination = 0;
		int type = 0;
		while (scan.hasNextLine())
		{
			scan.nextLine();
			if(scan.hasNextInt())
			{
				source = scan.nextInt();
				destination = scan.nextInt();
				type = scan.nextInt();
				if (type == 0)
				{
					Hashtable<Integer, Double> stopTable = new Hashtable<>();
					stopTable = this.hashTable.get(source);
					stopTable.put(destination, 2.0);
					this.hashTable.put(source, stopTable);
					this.E++;
				}
				else if (type == 2)
				{
					double cost = scan.nextDouble()/100;
					Hashtable<Integer, Double> stopTable = new Hashtable<>();
					stopTable = this.hashTable.get(source);
					stopTable.put(destination, cost);
					this.hashTable.put(source, stopTable);
					this.E++;
				}
			}
		}
		scan.close();
	}
	
	public boolean isDirectlyConnected(int departureStop, int arrivalStop)
	{
		Hashtable<Integer, Double> departureTable = new Hashtable<>();
		departureTable = this.hashTable.get(departureStop);
		if (departureTable.get(arrivalStop) != null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public double costFrom(int departureStop, int arrivalStop)
	{
		if(isDirectlyConnected(departureStop, arrivalStop))
		{
			Hashtable<Integer, Double> departureTable = new Hashtable<>();
			departureTable = this.hashTable.get(departureStop);
			return departureTable.get(arrivalStop);
		}
		else
		{
			return -1;
		}
	}
	
	public boolean isAStop(int stop)
	{
		if(this.hashTable.containsKey(stop))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
