public class IdSort {

    private TimeStore[] arr2;

    IdSort(TimeStore[] arr, int size) {
        arr2 = arr;
        int n = size;
        for (int i = 1; i < n; ++i) {
            int key = arr2[i].getTripId();
            TimeStore keyValues = arr2[i];
            int j = i - 1;


            while (j >= 0 && arr2[j].getTripId() > key) {
                arr2[j + 1] = arr2[j];
                j = j - 1;
            }
            arr2[j + 1] = keyValues;
        }
    }

    TimeStore[] getSortedArray(){
        return arr2;
    }

}
