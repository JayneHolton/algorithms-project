import java.io.FileNotFoundException;
import java.util.Hashtable;
import java.util.Set;
import java.util.Iterator;


public class ShortestRoute {

	public static final double INFINITY = 100000000;
	
	
	//Returns true if shortest path exists, returns false otherwise.
	public static boolean shortestPath(int source, int destination) throws FileNotFoundException
	{
		Graph g = new Graph("stops.txt","stop_times.txt","transfers.txt");
		
		//Checking if the stops are real.
		
		if(g.isAStop(source) == false && g.isAStop(destination) == false)
		{
			System.out.println("The bus stops " + source + " and " + destination + " do not exist.");
			return false;
		}
        if(g.isAStop(source) == false)
		{
			System.out.println("The bus stop " + source + " does not exist.");
			return false;
		}
		if(g.isAStop(destination) == false)
		{
			System.out.println("The bus stop " + destination + " does not exist.");
			return false;
		}		
		//Instantiate needed hash tables for djikstra's algorithm 
		Hashtable<Integer, Double> shortestDistance = new Hashtable<>();
		Hashtable<Integer, Boolean> visited = new Hashtable<>();
		Hashtable<Integer, Integer> previous = new Hashtable<>();
		g.hashTable.forEach((key,value)-> {
			shortestDistance.put(key,INFINITY);
		});
		g.hashTable.forEach((key,value)-> {
			visited.put(key,false);
		});
		g.hashTable.forEach((key,value)-> {
			previous.put(key,-1);
		});
		shortestDistance.put(source,0.0);
		
		boolean finished = false;
		while(!finished)
		{
			//Finding closest unvisited value to source
			int closestStop = 0;
			double closestDistance = INFINITY;
			Set<Integer> keys = shortestDistance.keySet();
			Iterator<Integer> itr = keys.iterator();
			
			while(itr.hasNext())
			{
				int currentKey = itr.next();
				if (shortestDistance.get(currentKey) < closestDistance && visited.get(currentKey) == false)
				{
					closestDistance = shortestDistance.get(currentKey);
					closestStop = currentKey;
				}
			}
			
			//Break if only impossible stops are left
			if (closestStop == 0)
			{
				break;
			}
			
			//Mark this stop as visited
			visited.replace(closestStop, true);
			
			//See if shorter path can be made with unvisited adjacent vertices
			Hashtable<Integer, Double> ht = new Hashtable<>();
			ht = g.hashTable.get(closestStop);
			keys = ht.keySet();
		    itr = keys.iterator();
		    
		    while(itr.hasNext())
		    {
		    	int currentKey = itr.next();
		    	if(shortestDistance.get(closestStop) + ht.get(currentKey) < shortestDistance.get(currentKey))
		    	{
		    		shortestDistance.replace(currentKey, shortestDistance.get(closestStop) + ht.get(currentKey));
		    		previous.replace(currentKey, closestStop);
		    	}
		    }
		    
		    //Check if all vertices have been visited
		    keys = visited.keySet();
		    itr = keys.iterator();
		    finished = true;
		    
		    while(itr.hasNext())
		    {
		    	if(visited.get(itr.next()) == false)
		    	{
		    		finished = false;
		    	}
		    }
		}
		double shortestDistanceValue = shortestDistance.get(destination);
		if(shortestDistanceValue < INFINITY)
		{
			String shortestPath = shortestRoute(previous, destination);
			System.out.println("The shortest path from stop " + source + " to stop " + destination +" will have a cost of " + shortestDistanceValue + ".\n");
			System.out.println(shortestPath);
			return true;
		}
		else
		{
			System.out.println("It is not possible to get from stop " + source + " to stop " + destination + ".");
			return false;
		}
	}
	
	public static String shortestRoute(Hashtable<Integer, Integer> previous, int destination)
	{
		String returnString = "The shortest path can be achieved by taking this route:\n";
		String route = "";
		int currentStop = destination;
		while(previous.get(currentStop) != -1)
		{
			route = previous.get(currentStop) + " -> " + route;
			currentStop = previous.get(currentStop);
		}
		returnString = returnString + route + destination + ".";
		return returnString;
	}
}