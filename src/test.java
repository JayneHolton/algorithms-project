import java.io.FileNotFoundException;

public class test {
	
	public static void main(String[] args) throws FileNotFoundException
	{
		//Stops exist but impossible, returns false
		ShortestRoute.shortestPath(646, 12034);
		
		//Source doesn't exist, returns false
		ShortestRoute.shortestPath(1, 646);
		
		//Destination doesn't exist, returns false
		ShortestRoute.shortestPath(646, 100000);
		
		//Both source and destination don't exist, returns false
		ShortestRoute.shortestPath(1, 100000);
		
		//Shortest path exists, returns true
		ShortestRoute.shortestPath(646, 12053);
	}
}
