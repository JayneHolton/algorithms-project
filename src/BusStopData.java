
public class BusStopData {


	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	String stopID, stopCode, stopName,stopDesc, stopLat, stopLon, zoneID, stopURL, locationType, parentStation, rearrangedStopName;
	
	@Override
    public String toString() {
        return  stopID + ", " + stopCode + ", " + stopName + ", " + stopDesc + ", " + stopLat + ", " + stopLon +", " +  zoneID + ", " + stopURL + ", " + locationType + ", " + parentStation;

    }
	
	BusStopData(String stopID, String stopCode, String stopName, String stopDesc, String stopLat, 
			String stopLon, String zoneID, String stopURL, String locationType, String parentStation, String rearrangedStopName){
		
		this.stopID = stopID;
		this.stopCode = stopCode;
		this.stopName = stopName;
		this.stopDesc = stopDesc;
		this.stopLat = stopLat;
		this.stopLon = stopLon;
		this.zoneID = zoneID;
		this.stopURL = stopURL;
		this.locationType = locationType;
		this.parentStation = parentStation;
		this.rearrangedStopName = rearrangedStopName;

		
	}
	
	
}


